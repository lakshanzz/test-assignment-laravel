<?php
/**
 * Created by PhpStorm.
 * User: Lakshan
 * Date: 26/09/18
 * Time: 3:17 PM
 */

namespace App\Http\Controllers\Web;


use App\Film;
use App\Genre;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class FilmsController extends Controller
{
    /**
     * @var Film
     */
    private $model;

    public function __construct(Film $filmModel)
    {
        $this->model = $filmModel;
    }

    /** Show list of films. Data grabs from API
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index(Request $request)
    {
        $page = $request->input('page',1);
        //get data from api
        $client = new Client(['base_uri' => 'http://localhost:8080/api']);
        $response = $client->request('GET', 'films?per_page=1&page='.$page);

        if($response->getStatusCode() != 200){
            abort("Failed to fetch data from API. Please try again.",500);
        }

        $data = json_decode($response->getBody());
        return view('films.films',compact('data'));
    }

    /** Show single film details
     * @param $slug Film slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $film = $this->model->with('genres','comments')->where('slug',$slug)->first();

        if(!$film)
            abort("Film not found",404);

        //dd($film);
        return view('films.film',compact('film'));
    }

    /** Show film creation form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('films.create');
    }

    /** Store film data when user submit form
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'title' => 'required|max:255',
            'description' => 'required',
            'release_date' => 'required|date',
            'rating' => 'required|in:1,2,3,4,5',
            'ticket_price' => 'required|numeric',
            'country' => 'required',
            'preview_image' => 'required|image',
            'genres' => 'required',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        //store uploaded file
        $preview = $request->file('preview_image');
        $stored_file_name = $this->storeUploadedFile($preview);

        // store the film
        $film_data = $request->all();
        $film_data['preview'] = "uploads/$stored_file_name";
        $film = $this->model->create($film_data);

        // attach film genres\
        $genres_vals = explode(',',$request->input('genres'));
        $genres = $this->getGenreIds($genres_vals);
        foreach ($genres as $genre_id) {
            $film->genres()->attach($genre_id);
        }

        session()->flash("Film created successfully.");

        return redirect(route('films.index'));
    }

    /** Find genre ids from inserted list of parses and return them
     * @param array $list
     * @return array
     */
    private function getGenreIds(array $list): array
    {
        $ids = [];

        foreach ($list as $genre_name) {

            $genre_name = trim($genre_name);

            //if genre exists get it
            $genre = Genre::where('genre',$genre_name)->first();

            if(!$genre){ // if genre not exists, create and get it
                $genre = Genre::create(['genre' => $genre_name]);
            }

            $ids[] = $genre->id;
        }

        //remove duplicates if exits
        return array_unique($ids);
    }

    /** Helper function to store uploaded image preview
     * @param UploadedFile $preview
     * @return string saved file name
     */
    private function storeUploadedFile(UploadedFile $preview): string
    {
        $original_file_name = $preview->getFilename();
        $extension = $preview->extension();
        $stored_file_name = time()."_$original_file_name.$extension";
        $preview->move(public_path('/uploads'),$stored_file_name);
        return $stored_file_name;
    }
}