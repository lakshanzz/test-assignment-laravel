<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FilmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 3;
        for ($i = 1; $i <= $count; $i++)
        {
            $faker = Faker::create();
            $film_id = \Illuminate\Support\Facades\DB::table('films')->insertGetId([
                'title' => $faker->sentence(3),
                'slug' => $faker->slug,
                'description' => $faker->text,
                'release_date' => $faker->date(),
                'rating' => random_int(1,5),
                'ticket_price' => $faker->randomFloat(2,10,100),
                'country' => $faker->country,
                'preview' => '',
                'created_at' => $faker->dateTime,
                'updated_at' => $faker->dateTime

            ]);

            \Illuminate\Support\Facades\DB::table('comments')->insert([
               'film_id' => $film_id,
               'name' => $faker->name,
                'comment' => $faker->text,
                'created_at' => $faker->dateTime,
                'updated_at' => $faker->dateTime
            ]);


        }

        $genres = ['Horror', 'Action', 'Romantic', 'Drama'];
        $genre_ids = [];
        foreach ($genres as $genre) {
            $genre_ids[] = \Illuminate\Support\Facades\DB::table('genres')->insertGetId(['genre'=>$genre]);
        }

        \App\Film::all()->each(function ($film) use ($genre_ids){
            $film->genres()->attach($genre_ids[random_int(0,3)]);
        });
    }
}
