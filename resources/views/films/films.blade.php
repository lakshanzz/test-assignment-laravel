@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Films  (Total : {{$data->total}}) | Page {{$data->current_page}} of {{$data->last_page}}</div>

                    <div class="card-body">
                        <ul class="list-unstyled">
                        @foreach($data->data as $film)
                                <li class="media">
                                    <img class="mr-3" src="https://via.placeholder.com/50x50" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="mt-0 mb-1"><a href="{{route('films.show',['id'=>$film->slug])}}">{{$film->title}}</a></h5>
                                        {{$film->description}}
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-2"><strong>Ticket Price</strong></div>
                                            <div class="col-md-4">${{$film->ticket_price}}</div>
                                            <div class="col-md-2"><strong>Release Date</strong></div>
                                            <div class="col-md-4">{{$film->release_date}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"><strong>Rating</strong></div>
                                            <div class="col-md-4">{{$film->rating}}/5</div>
                                            <div class="col-md-2">Country</div>
                                            <div class="col-md-4">{{$film->country}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"><strong>Genres</strong></div>
                                            <div class="col-md-4">
                                                @foreach($film->genres as $genre)
                                                    <span class="badge badge-primary">{{$genre->genre}}</span>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </li>
                        @endforeach
                        </ul>


                    </div>
                </div>

                <nav aria-label="Page navigation" style="margin-top: 20px">
                    <ul class="pagination">
                        @if($data->last_page_url)<li class="page-item"><a class="page-link" href="{{route('films.index')}}?page={{$data->current_page - 1}}">Previous</a></li>@endif
                        @if($data->next_page_url)<li class="page-item"><a class="page-link" href="{{route('films.index')}}?page={{$data->current_page + 1}}">Next</a></li>@endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endsection
