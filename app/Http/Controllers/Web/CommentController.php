<?php
/**
 * Created by PhpStorm.
 * User: Lakshan
 * Date: 26/09/18
 * Time: 4:41 PM
 */

namespace App\Http\Controllers\Web;


use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    private $model;

    public function __construct(Comment $commentModel)
    {
        $this->model = $commentModel;
    }


    public function store(Request $request)
    {
        if(!Auth::check())
            abort(403, "Permission denied.");

        $validator = \Validator::make($request->all(),[
           'name' => 'required',
           'comment' => 'required',
            'film_id' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $this->model->create($request->all());
        session()->flash("Film published successfully.");
        return redirect()->back();
    }
}