@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Film: {{$film->title}}</div>
                    <div class="card-body">
                        {{$film->description}}
                        <hr>
                        <div class="row">
                            <div class="col-md-2"><strong>Ticket Price</strong></div>
                            <div class="col-md-4">${{$film->ticket_price}}</div>
                            <div class="col-md-2"><strong>Release Date</strong></div>
                            <div class="col-md-4">{{$film->release_date}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"><strong>Rating</strong></div>
                            <div class="col-md-4">{{$film->rating}}/5</div>
                            <div class="col-md-2">Country</div>
                            <div class="col-md-4">{{$film->country}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"><strong>Genres</strong></div>
                            <div class="col-md-4">
                                @foreach($film->genres as $genre)
                                    <span class="badge badge-primary">{{$genre->genre}}</span>
                                @endforeach
                            </div>
                        </div>
                    </div>


                </div>

                <div class="card" style="margin-top: 20px">
                    <div class="card-header">Comments ({{count($film->comments)}})</div>
                    <div class="card-body">
                        @if(Auth::check())
                            <form method="POST" action="{{ route('comments.store') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label text-md-right">{{ __('Your Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="comment" class="col-md-4 col-form-label text-md-right">{{ __('Comment') }}</label>

                                    <div class="col-md-6">
                                        <textarea id="comment" name="comment" class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}" rows="3" required></textarea>

                                        @if ($errors->has('comment'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <input type="hidden" name="film_id" value="{{$film->id}}">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Add Comment') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @endif

                            <hr>

                            @foreach($film->comments as $comment)
                                <div class="media">
                                    <img class="mr-3" src="https://via.placeholder.com/50x50" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="mt-0">{{$comment->name}}</h5>
                                        {{$comment->comment}}
                                        <br><small>Published at: {{$comment->created_at}}</small>
                                    </div>
                                </div>
                                <hr>
                            @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
