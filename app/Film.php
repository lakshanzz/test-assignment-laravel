<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use Sluggable;

    protected $table = 'films';

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $fillable = [
        'title', 'slug', 'description', 'release_date', 'ticket_price', 'rating', 'country', 'preview'
    ];

    public function genres()
    {
        return $this->belongsToMany(Genre::class,'film_genres','film_id','genre_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'film_id');
    }
}
