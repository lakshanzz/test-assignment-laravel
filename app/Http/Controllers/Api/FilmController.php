<?php
/**
 * Created by PhpStorm.
 * User: Lakshan
 * Date: 26/09/18
 * Time: 12:35 PM
 */

namespace App\Http\Controllers\Api;


use App\Film;
use App\Genre;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class FilmController extends Controller
{
    /**
     * @var Film
     */
    private $model;

    public function __construct(Film $filmModel)
    {
        $this->model = $filmModel;
    }

    /** Show list of films
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        $items_per_page = $request->input('per_page',10);
        return $this->model->with('genres')->paginate($items_per_page);
    }

    /** Show single film by id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $film = $this->model->with('genres','comments')->find($id);

        if(!$film)
            return response()->json(['error' => 'Film not found',404]);

        return $film;
    }

    /** Saves a film
     * @param Request $request
     * @return Film
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'title' => 'required|max:255',
            'description' => 'required',
            'release_date' => 'required|date',
            'rating' => 'required|in:1,2,3,4,5',
            'ticket_price' => 'required|numeric',
            'country' => 'required',
            'preview_image' => 'required|file|image',
            'genres' => 'required|array',
            'genres.*' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),400);
        }

        //store uploaded file
        $preview = $request->file('preview_image');
        $stored_file_name = $this->storeUploadedFile($preview);

        // store the film
        $film_data = $request->all();
        $film_data['preview'] = "uploads/$stored_file_name";
        $film = $this->model->create($film_data);

        // attach film genres
        $genres = $this->getGenreIds($request->input('genres'));
        foreach ($genres as $genre_id) {
            $film->genres()->attach($genre_id);
        }

        return $this->model->with('genres')->find($film->id);
    }

    /** Updates a film
     * @param $id
     * @param Request $request
     * @return Film
     */
    public function update($id, Request $request)
    {
        $film = $this->model->find($id);
        if(!$film)
            return response()->json(['error' => 'Film not found',404]);

        $validator = \Validator::make($request->all(),[
            'title' => 'sometimes|max:255',
            'release_date' => 'sometimes|date',
            'rating' => 'sometimes|in:1,2,3,4,5',
            'ticket_price' => 'sometimes|numeric',
            'preview_image' => 'sometimes|file|image',
            'genres' => 'sometimes|array'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),400);
        }

        $update_data = $request->all();

        if($request->file('preview_image') && $request->file('preview_image')->isFile())
        {
            $preview = $request->file('preview_image');
            $stored_file_name = $this->storeUploadedFile($preview);
            $update_data['preview'] = "uploads/$stored_file_name";
        }

        $film->update($update_data);

        // if genres exists to update
        if(is_array($request->input('genres'))){
            $film->genres->detach();

            $genres = $this->getGenreIds($request->input('genres'));
            foreach ($genres as $genre_id) {
                $film->genres()->attach($genre_id);
            }
        }

        return $this->model->with('genres')->find($film->id);
    }

    /** Deletes a Film
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $film = $this->model->find($id);
        if(!$film)
            return response()->json(['error' => 'Film not found',404]);

        $film->delete();

        return response()->json(['message' =>'success']);
    }

    /** Find genre ids from inserted list of parses and return them
     * @param array $list
     * @return array
     */
    private function getGenreIds(array $list): array
    {
        $ids = [];

        foreach ($list as $genre_name) {
            //if genre exists get it
            $genre = Genre::where('genre',$genre_name)->first();

            if(!$genre){ // if genre not exists, create and get it
                $genre = Genre::create(['genre' => $genre_name]);
            }

            $ids[] = $genre->id;
        }

        //remove duplicates if exits
        return array_unique($ids);
    }

    /** Helper function to store uploaded image preview
     * @param UploadedFile $preview
     * @return string saved file name
     */
    private function storeUploadedFile(UploadedFile $preview): string
    {
        $original_file_name = $preview->getFilename();
        $extension = $preview->extension();
        $stored_file_name = time()."_$original_file_name.$extension";
        $preview->move(public_path('/uploads'),$stored_file_name);
        return $stored_file_name;
    }
}