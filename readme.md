#Test Assignment - Laravel

##API Docs
---
*GET `/api/films`*

Get list of films

Accepted Parameters:

Parameter | Description
--- | ---
`page` | Page Number
`per_page` | Items to show per page

Response

```
{
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "title": "Nihil voluptates numquam dolorem.",
            "slug": "aut-tempore-sit-eveniet-architecto-nobis-nisi",
            "description": "Ut ea consequatur occaecati beatae facilis minus voluptates. Quasi occaecati voluptatem quae ut eius.",
            "release_date": "1974-11-24",
            "rating": 1,
            "ticket_price": "56.19",
            "country": "Canada",
            "preview": "",
            "created_at": null,
            "updated_at": null,
            "genres": [
                {
                    "id": 1,
                    "genre": "Horror",
                    "pivot": {
                        "film_id": 1,
                        "genre_id": 1
                    }
                }
            ]
        }
    ],
    "first_page_url": "http://localhost:8000/api/films?page=1",
    "from": 1,
    "last_page": 14,
    "last_page_url": "http://localhost:8000/api/films?page=14",
    "next_page_url": "http://localhost:8000/api/films?page=2",
    "path": "http://localhost:8000/api/films",
    "per_page": "1",
    "prev_page_url": null,
    "to": 1,
    "total": 14
}
```
---
*GET `/api/films/{FILM_ID}`*

Get a single film data

Response
```
{
    "id": 1,
    "title": "Nihil voluptates numquam dolorem.",
    "slug": "aut-tempore-sit-eveniet-architecto-nobis-nisi",
    "description": "Ut ea consequatur occaecati beatae facilis minus voluptates. Quasi occaecati voluptatem quae ut eius.",
    "release_date": "1974-11-24",
    "rating": 1,
    "ticket_price": "56.19",
    "country": "Canada",
    "preview": "",
    "created_at": null,
    "updated_at": null,
    "genres": [
        {
            "id": 1,
            "genre": "Horror",
            "pivot": {
                "film_id": 1,
                "genre_id": 1
            }
        }
    ],
    "comments": [
        {
            "id": 1,
            "film_id": 1,
            "name": "Winnifred Braun",
            "comment": "Incidunt dolores perferendis et et. Nostrum ea sapiente non libero consequatur aliquam repellat dolor. Omnis cum quisquam praesentium voluptatem. Sint inventore minus in placeat.",
            "created_at": "2018-09-18 00:00:00",
            "updated_at": null
        }
    ]
}
```
---
*POST `/api/films/{FILM_ID}`*

Add new film

Accepted Parameters:

Parameter | Description
--- | ---
`title` | Film Title
`description` | Film description
`release_date` | Film release date
`rating` | Film rating. Values 1 to 5
`ticket_price` | Ticket price
`country` | Film originated country
`preview_image` | Image file of preview image.
`generes` | Array of film genres. 

Response 

```$xslt
{
    "id": 1,
    "title": "Nihil voluptates numquam dolorem.",
    "slug": "aut-tempore-sit-eveniet-architecto-nobis-nisi",
    "description": "Ut ea consequatur occaecati beatae facilis minus voluptates. Quasi occaecati voluptatem quae ut eius.",
    "release_date": "1974-11-24",
    "rating": 1,
    "ticket_price": "56.19",
    "country": "Canada",
    "preview": "",
    "created_at": null,
    "updated_at": null,
    "genres": [
        {
            "id": 1,
            "genre": "Horror",
            "pivot": {
                "film_id": 1,
                "genre_id": 1
            }
        }
    ],
}
```
---
*PATCH `/api/films/{FILM_ID}`*

Update an existing film

Accepted Parameters:

Parameter | Description
--- | ---
`title` | Film Title
`description` | Film description
`release_date` | Film release date
`rating` | Film rating. Values 1 to 5
`ticket_price` | Ticket price
`country` | Film originated country
`preview_image` | Image file of preview image.
`generes` | Array of film genres. 

Response 

```$xslt
{
    "id": 1,
    "title": "Nihil voluptates numquam dolorem.",
    "slug": "aut-tempore-sit-eveniet-architecto-nobis-nisi",
    "description": "Ut ea consequatur occaecati beatae facilis minus voluptates. Quasi occaecati voluptatem quae ut eius.",
    "release_date": "1974-11-24",
    "rating": 1,
    "ticket_price": "56.19",
    "country": "Canada",
    "preview": "",
    "created_at": null,
    "updated_at": null,
    "genres": [
        {
            "id": 1,
            "genre": "Horror",
            "pivot": {
                "film_id": 1,
                "genre_id": 1
            }
        }
    ],
}
```
---
*DELETE `/api/films/{FILM_ID}`*

Deletes a film